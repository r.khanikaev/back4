<!DOCTYPE html>
<html lang="ru">
    
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Backend_4</title>
    <link rel="stylesheet" href="style.css">
    <script src="script.js"></script>
  </head>
<body>
    <?php
    if (!empty($messages)) {
        print('<div id="messages">');
        // Выводим все сообщения.
        foreach ($messages as $message) {
            print($message);
        }
        print('</div>');
    }
    // Далее выводим форму отмечая элементы с ошибками классом error
    // и задавая начальные значения элементов ранее сохраненными.
    ?>
    <div class="form">
            <form action="" method="POST">
                <label><input name="fio" type="text"  placeholder="Name" <?php if ($errors['fio']) {print 'class="error_text"';} ?> value="<?php print $values['fio']; ?>" /></label>
                   <label><input name="email" type="email" placeholder="E-mail" <?php if ($errors['email']) {print 'class="error_text"';} ?> value="<?php print $values['email']; ?>" /></label>
                   <label><input type="date" id="date" name="birthday" <?php if ($errors['birthday']) {print 'class="error_text"';} ?> value="<?php print $values['birthday']; ?>" /></label>
                
                <?php if ($errors['gender']) {print '<div class="error_text">';} ?> 
                <p>Пол:</p>
                <div class="form_radio" >
                    <label><input type="radio" name="gender" value="Male" <?php if($values['gender']=='Male') print "checked";else print ""; ?> />
                       М
                    </label>
                </div>
                
                <div class="form_radio">
                    <label><input type="radio" name="gender" value="Female" <?php echo ($values['gender']=='Female') ?  "checked" : "" ;  ?> />
                        Ж
                    </label>
                </div>
                
                <?php if ($errors['limbs']) {print '<div class="error_text">';} ?> 
                <p>Количество конечностей:</p>
                <div class="form_radio">
                    <label><input type="radio" name="limbs" value="one" <?php echo ($values['limbs']=='one') ?  "checked" : "" ;  ?> />
                        1
                    </label>
                </div>
                
                <div class="form_radio">
                    <label><input type="radio" name="limbs" value="two" <?php echo ($values['limbs']=='two') ?  "checked" : "" ;  ?> />
                        2
                    </label>
                </div>
                
                <div class="form-group2">
                <p> <select name="ability[]" multiple="multiple">
                    <?php if ($errors['ability']) {print 'class="error_text"';} ?>
                    <option value="1" <?php echo (in_array("1",$ability)) ?  "selected" : ""  ; ?>>бессмертие</option>
                    <option value="2" <?php echo (in_array('2',$ability)) ?  "selected" : ""  ; ?>>Прохождение сквозь стены</option>
                    <option value="3" <?php echo (in_array('3',$ability)) ?  "selected" : ""  ; ?>>Левитация </option>
                    <option value="4" <?php echo (in_array('4',$ability)) ?  "selected" : ""  ; ?>>Управление временем</option>
                    </select>
                </div>
                    
                    <textarea name="biography" cols="26" rows="2" placeholder= "Биография" ></textarea>
                    <input type="checkbox" name="contract" value="allow"> <p>C контрактом ознакомлен</p>
                    <input class="submit1" type='submit' value='Submit'>
            </form>
    </div>
</body>
</html>

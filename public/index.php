<?php
/**
 * Реализовать проверку заполнения обязательных полей формы в предыдущей
 * с использованием Cookies, а также заполнение формы по умолчанию ранее
 * введенными значениями.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    // Если есть параметр save, то выводим сообщение пользователю.
    $messages[] = '<div style="color: white;">Спасибо, результаты сохранены.</div>';
  }

  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['birthday'] = !empty($_COOKIE['birthday_error']);
  $errors['contract'] = !empty($_COOKIE['contract_error']);
  $errors['gender'] = !empty($_COOKIE['gender_error']);
  $errors['limbs'] = !empty($_COOKIE['limbs_error']);
  $errors['ability'] = !empty($_COOKIE['ability_error']);
  // TODO: аналогично все поля.

    // Выдаем сообщения об ошибках.
    if ($errors['fio']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('fio_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="error">Заполните имя.</div>';
    }  
    
    if ($errors['email']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('email_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="error">Заполните email.</div>';
    }  
    
    if ($errors['birthday']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('birthday_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="error">Заполните дату.</div>';
    }
    
    if ($errors['contract']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('contract_error', '', 100000);
        // Выводим сообщение.
        $messages[6] = '<div class="error">Ознакомьтесь с контрактом.</div>';
    }    
    
    if ($errors['gender']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('gender_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="error">Укажите свой пол.</div>';
    }     
    
    if ($errors['limbs']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('limbs_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="error">Укажите кол-во конечностей.</div>';
    }   
    
    if ($errors['ability']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('ability_error', '', 100000);
        // Выводим сообщение.
        $messages[5] = '<div class="error_text">Должна быть выбрана хотя бы одна способность.</div>';
    }


    
  // TODO: тут выдать сообщения об ошибках в других полях.

  // Складываем предыдущие значения полей в массив, если есть.
  $values = array();
  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['birthday'] = empty($_COOKIE['birthday_value']) ? '' : $_COOKIE['birthday_value'];
  $values['contract'] = empty($_COOKIE['contract_value']) ? '' : $_COOKIE['contract_value'];
  $values['gender'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
  $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
  // TODO: аналогично все поля.
    
    $ability = array();
    $ability = empty($_COOKIE['ability_values']) ? array() : unserialize($_COOKIE['ability_values'], ["allowed_classes" => false]);
    // TODO: аналогично все поля.
    // Включаем содержимое файла form.php.
    // В нем будут доступны переменные $messages, $errors и $values для вывода 
    // сообщений, полей с ранее заполненными данными и признаками ошибок.
    include('form.php');
}

// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
  $errors = FALSE;
  if (empty($_POST['fio'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
  }  
    
    if (empty($_POST['email'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
  }  
    
    if (empty($_POST['birthday'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('birthday_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('birthday_value', $_POST['birthday'], time() + 30 * 24 * 60 * 60);
  }
    
    if (empty($_POST['contract'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('contract_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('contract_value', $_POST['contract'], time() + 30 * 24 * 60 * 60);
  }
    
    if (empty($_POST['gender'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('gender_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('gender_value', $_POST['gender'], time() + 30 * 24 * 60 * 60);
  }
    
    if (empty($_POST['limbs'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('limbs_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
  }
    
    if (!isset($_POST['ability'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('ability_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        
        setcookie('ability_values', serialize($_POST['ability']), time() + 30 * 24 * 60 * 60);
    }
    

// *************
// TODO: тут необходимо проверить правильность заполнения всех остальных полей.
// Сохранить в Cookie признаки ошибок и значения полей.
// *************

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    // TODO: тут необходимо удалить остальные Cookies.
  }  
  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
    else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('email_error', '', 100000);
    // TODO: тут необходимо удалить остальные Cookies.
  }  
    
if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
    else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('date_error', '', 100000);
    // TODO: тут необходимо удалить остальные Cookies.
  }
    if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
    else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('contract_error', '', 100000);
    // TODO: тут необходимо удалить остальные Cookies.
  }   
    if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
    else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('gender_error', '', 100000);
    // TODO: тут необходимо удалить остальные Cookies.
  }     
    if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
    else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('limbs_error', '', 100000);
    // TODO: тут необходимо удалить остальные Cookies.
  }    

$user = 'u20968';
$pass = '2666052';
$db = new PDO('mysql:host=localhost;dbname=u20968', $user, $pass, array(PDO::ATTR_PERSISTENT => true));


// Подготовленный запрос. Не именованные метки.
try {
  $stmt = $db->prepare("INSERT INTO user SET fio = ?, user_email = ?, user_birthday = ?, user_gender = ? , user_limb_count = ?, user_biography = ?");
  $stmt -> execute([$_POST['fio'],$_POST['email'],$_POST['birthday'],$_POST['gender'],$_POST['limbs'],$_POST['biography']]);

  $stmt2 = $db->prepare("INSERT INTO link SET userr_id= ?, abil_id = ?");
  $id = $db->lastInsertId();
  foreach ($_POST['ability'] as $s)
    $stmt2 -> execute([$id, $s]);
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}

//  stmt - это "дескриптор состояния".
 
//  Именованные метки.
//$stmt = $db->prepare("INSERT INTO test (label,color) VALUES (:label,:color)");
//$stmt -> execute(array('label'=>'perfect', 'color'=>'green'));
 
//Еще вариант
/*$stmt = $db->prepare("INSERT INTO users (firstname, lastname, email) VALUES (:firstname, :lastname, :email)");
$stmt->bindParam(':firstname', $firstname);
$stmt->bindParam(':lastname', $lastname);
$stmt->bindParam(':email', $email);
$firstname = "John";
$lastname = "Smith";
$email = "john@test.com";
$stmt->execute();
*/

// Делаем перенаправление.
// Если запись не сохраняется, но ошибок не видно, то можно закомментировать эту строку чтобы увидеть ошибку.
// Если ошибок при этом не видно, то необходимо настроить параметр display_errors для PHP.

// Сохраняем куку с признаком успешного сохранения.
setcookie('save', '1');

header('Location: ?save=1');
}


